# testcreatepod

[![CI Status](https://img.shields.io/travis/tudoan_GCS/testcreatepod.svg?style=flat)](https://travis-ci.org/tudoan_GCS/testcreatepod)
[![Version](https://img.shields.io/cocoapods/v/testcreatepod.svg?style=flat)](https://cocoapods.org/pods/testcreatepod)
[![License](https://img.shields.io/cocoapods/l/testcreatepod.svg?style=flat)](https://cocoapods.org/pods/testcreatepod)
[![Platform](https://img.shields.io/cocoapods/p/testcreatepod.svg?style=flat)](https://cocoapods.org/pods/testcreatepod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

testcreatepod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'testcreatepod'
```

## Author

tudoan_GCS, c.tu.anh.doan@zonarsystems.com

## License

testcreatepod is available under the MIT license. See the LICENSE file for more info.
