#
# Be sure to run `pod lib lint testcreatepod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'testcreatepod'
  s.version          = '1.0.3'
  s.summary          = 'AAA'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = "ABCD"

  s.homepage         = 'https://gitlab.com/tudoan_GCS/testcreatepod'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'tudoan_GCS' => 'c.tu.anh.doan@zonarsystems.com' }
  s.source           = { :git => 'git@gitlab.com:tudoan_GCS/testcreatepod.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '11.0'
  s.swift_version           = '4.0'
  s.source_files = 'testcreatepod/Classes/**/*'

  # s.resource_bundles = {
  #   'testcreatepod' => ['testcreatepod/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
